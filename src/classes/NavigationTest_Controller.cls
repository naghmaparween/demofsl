global class NavigationTest_Controller {
    public string UITheme {get;set;}
    public  String AccountId {get;set;}
    
    public NavigationTest_Controller(){
        UITheme = UserInfo.getUiThemeDisplayed();
        AccountId = '0017F00000RCz56';
    }
    
    Public PageReference NavigatetoAccount(){
        PageReference redirectPage = new PageReference('/'+'0017F00000RCz56' );
        redirectPage.setRedirect(true);
        //redirectPage.getParameters().put('id',);
        return redirectPage;        
    }
    
    webservice static string NavigatetoAccLight(){
        //Logic to obtain account ID
       string AccountId1 = '0017F00000RCz56';
        return AccountId1;
    }

}